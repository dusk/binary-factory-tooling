python checkout-repository.py --repository https://invent.kde.org/packaging/craftmaster --into "C:/Craft/CI-Qt515/" --branch master
python checkout-repository.py --repository https://invent.kde.org/sysadmin/ci-tooling --into "C:/Craft/CI-Qt515/" --branch master
python checkout-repository.py --repository https://invent.kde.org/sysadmin/binary-factory-tooling --into "C:/Craft/CI-Qt515/" --branch master

cd C:\Craft\CI-Qt515\craftmaster\
python Craftmaster.py --config ../binary-factory-tooling/craft/configs/master/CraftBinaryCache.ini --config-override ../binary-factory-tooling/craft/CIBuilderDeployConfig.ini --target=windows-msvc2019_64-cl-debug -c -i --no-cache craft
python Craftmaster.py --config ../binary-factory-tooling/craft/configs/master/CraftBinaryCache.ini --config-override ../binary-factory-tooling/craft/CIBuilderDeployConfig.ini --target=windows-msvc2019_64-cl-debug -c --list-file ../ci-tooling/craftmaster/packages.list
